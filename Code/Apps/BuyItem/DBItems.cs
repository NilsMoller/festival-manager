﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using MySql.Data;
using Database;
using Database.Model;

namespace BuyItem
{
    class DBItems
    {

        public MySqlConnection connection;

        public DBItems()
        {
            string connInfo = "server=studmysql01.fhict.local;" +
                    "database=dbi380048;" +
                    "user id=dbi380048;" +
                    "password=pcsb;";
            connection = new MySqlConnection(connInfo);
        }

        public List<Customer> GetAccountsInfo()
        {
            List<Customer> temp = new List<Customer>();
            string sql = "SELECT * FROM accounts";
            MySqlCommand command = new MySqlCommand(sql, connection);
            try
            {
                connection.Open();
                Console.WriteLine("Got Connection");
                MySqlDataReader reader = command.ExecuteReader();
                Console.WriteLine("Started reader");
                string accountemail;
                string accountid;
                int creditStored;
                while (reader.Read())
                {
                    Console.WriteLine("reader started");
                    accountemail = Convert.ToString(reader["Email"]);
                    Console.WriteLine("Got Email");
                    accountid = Convert.ToString(reader["RFID"]);
                    Console.WriteLine("Got RFID");
                    creditStored = Convert.ToInt32(reader["CreditStored"]);
                    Console.WriteLine("Got credit:" + creditStored);
                    temp.Add(new Customer(accountemail, accountid, creditStored));

                }
            }
            catch
            {
                MessageBox.Show("Error while loading...");
            }
            finally
            {
                connection.Close();
            }
            return temp;
        }

        public Account GetAccount(string tagId)
        {
            using (var context = new RockfairContext())
            {
                return context.Accounts.FirstOrDefault(acc => acc.RFID == tagId);
            }
        }

        public void UpdateAccount(Account acc)
        {
            using (var context = new RockfairContext())
            {
                context.Accounts.FirstOrDefault(accc => accc.RFID == acc.RFID).CreditStored = acc.CreditStored;
                context.SaveChanges();
            }
        }
    }
}
