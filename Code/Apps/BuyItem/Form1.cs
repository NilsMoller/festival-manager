﻿using Database;
using MySql.Data.MySqlClient;
using Phidget22;
using Phidget22.Events;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace BuyItem
{
    public partial class Form1 : Form
    {
        string tag;
        private RFID rfid;
        private List<Product> products = new List<Product>();
        private string buyerTag;
        DBItems dbItems;
        private MySqlConnection connection;
        double TotalPrice;

        public Form1()
        {
            InitializeComponent();
            dbItems = new DBItems();
            UpdateProducts();

            try
            {
                rfid = new RFID();
                rfid.Open();
                rfid.Attach += RFIDstuffs;
                rfid.Tag += RFIDTagFound;
                rfid.Tag += new RFIDTagEventHandler(ProcessThisTag);
                this.buyerTag = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private double GetCredit()
        {
            foreach (Customer c in dbItems.GetAccountsInfo())
            {
                if (c.AccountId == tag)
                {
                    return c.Credit;
                }
            }
            return 0;
        }

        private void RFIDTagFound(object sender, RFIDTagEventArgs e)
        {
            if (this.lBoxProducts.Items.Count <= 0)
            {
                return;
            }
            //TODO : Insert tag checking logic here :)
            this.buyerTag = e.Tag;
            this.RFIDpayment(sender, e);
        }


        private void RFIDstuffs(object sender, AttachEventArgs e)
        {
            RFID attachedDevice = (RFID)sender;
            try
            {
                rfid.AntennaEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ProcessThisTag(object sender, RFIDTagEventArgs e)
        {
            try
            {
                if (SearchRFID(e.Tag) == null)
                {
                    MessageBox.Show($"Account{ Environment.NewLine }Unregistered");
                }
                else
                {
                    tbID.Text = SearchRFID(e.Tag).AccountId;
                    tag = SearchRFID(e.Tag).AccountId;
                    tbAccName.Text = SearchRFID(e.Tag).Email;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Something wrong...");
            }
            if (tbAccName == null)
            {
                MessageBox.Show("Something wrong...");

            }
        }

        public void RFIDpayment(object sender, RFIDTagEventArgs e)
        {
            var acc = dbItems.GetAccount(e.Tag);
            tbID.Text = acc.RFID;
            tbAccName.Text = acc.Email;

            if (TotalPrice > acc.CreditStored)
            {
                MessageBox.Show("Please make sure that you have money in you account!");
            }
            else
            {
                acc.CreditStored -= TotalPrice;
                dbItems.UpdateAccount(acc);
                MessageBox.Show("Payment was succesful!");

            }

        }

        private Customer SearchRFID(string id)
        {
            List<Customer> temp = dbItems.GetAccountsInfo();
            foreach (Customer c in temp)
            {
                if (c.GetAccountId() == id)
                {
                    return c;
                }
            }
            return null;
        }

        private void UpdateProducts()
        {
            //How to use entity framework
            using (var context = new RockfairContext())
            {
                products = context.Consumables.ToList().Select(x => new Product(x.Id, x.Name, x.Price)).ToList();
            }
            cBoxProducts.Items.Clear();

            //foreach (var item in products)
            //{
            //    cBoxProducts.Items.Add(item);
            //}

            cBoxProducts.Items.AddRange(products.ToArray());
        }

        private void Total()
        {
            double price = 0;

            foreach (var item in lBoxProducts.Items)
            {
                Product product = (Product)item;
                price += product.Price;
            }
            TotalPrice = price;
            lbPrice.Text = price.ToString("F2");
        }

        private void cBoxProducts_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedProduct = (Product)cBoxProducts.Items[cBoxProducts.SelectedIndex];

            tbPrice.Text = Convert.ToString(selectedProduct.Price);
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            var selectedProduct = (Product)cBoxProducts.Items[cBoxProducts.SelectedIndex];

            for (int i = 0; i < UpDown.Value; i++)
            {
                lBoxProducts.Items.Add(selectedProduct);
            }
            Total();
        }

        private void btRemove_Click(object sender, EventArgs e)
        {
            lBoxProducts.Items.RemoveAt(lBoxProducts.SelectedIndex);
            Total();
        }

        private void lBoxProducts_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lBoxProducts.SelectedIndex == -1)
            {
                btRemove.Enabled = false;
            }
            else
            {
                btRemove.Enabled = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            tbID.Clear();
            tbAccName.Clear();
            tbPrice.Clear();
            lBoxProducts.Items.Clear();
            lbPrice.Text = "0.00";
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            rfid.Close();
        }
        
    }
}
