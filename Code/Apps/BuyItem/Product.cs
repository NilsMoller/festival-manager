﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuyItem
{
    class Product
    {
        public double Price { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }

        public Product(int id, string newname, double newprice)
        {
            Id = id;
            Name = newname;
            Price = newprice;
        }


        public override string ToString()
        {
            return string.Format("{0} ({1:F2})", Name, Price);
        }
    }
}
