﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;
using Phidget22;
using Phidget22.Events;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Security.Cryptography;

namespace ATMLog
{
    public partial class bt10 : Form
    {
        MySqlConnection connection = new MySqlConnection("server=studmysql01.fhict.local;" +
                                                          "database=dbi380048;" +
                                                          "user id=dbi380048;" +
                                                          "password=pcsb;");
        private RFID myRFIDReader;
        
        private string connectedTag;
        string email;
        string text;
        int credit;
        int depositID = 0;
        public bt10()
        {
            InitializeComponent();
            try
            {
                myRFIDReader = new RFID();
                myRFIDReader.Attach += MyRFIDReader_Attach;
                myRFIDReader.Detach += MyRFIDReader_Detach;
                myRFIDReader.Tag += MyRFIDReader_Tag;
                myRFIDReader.TagLost += MyRFIDReader_TagLost;
                myRFIDReader.Open();

            }
            catch (PhidgetException ex)
            {
                lblRFIDstatus.Text = "Could not startup!";
            }
        }
        private void MyRFIDReader_TagLost(object sender, RFIDTagLostEventArgs e)
        {
            
            lblRFIDstatus.Text = "No tag...";
        }

        private void MyRFIDReader_Tag(object sender, RFIDTagEventArgs e)
        {      
            lblRFIDstatus.Text = $"Tag: {e.Tag} found!";
            connectedTag = e.Tag;
        }

        private void MyRFIDReader_Detach(object sender, DetachEventArgs e)
        {
            lblRFIDstatus.Text = "Detached RFID-Reader.";
        }

        private void MyRFIDReader_Attach(object sender, AttachEventArgs e)
        {
            lblRFIDstatus.Text = "Attached RFID-Reader.";
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
                   
            try
            {
                connection.Open();
                string query = $"SELECT  Email  FROM accounts WHERE RFID =  '" + connectedTag + "' ;";

                MySqlCommand command = new MySqlCommand(query, connection);
                //MySqlDataReader reader = command.ExecuteReader();
                //var check = reader.Read(); 
                
                email = (string)command.ExecuteScalar();
                tbaccountidcode.Text = email;
            }
            catch (PhidgetException ex)
            {
                lblRFIDstatus.Text = "Could not connect to the RFID-Reader!";
            }
            finally
            {
                connection.Close();
            }
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            //manual transaction button
            try
            {
                connection.Open(); //new column named Paid For Ticket , otherwise doesn't  make any sense
                string query = $"SELECT CreditStored FROM accounts WHERE Email ='" + email + "';";
                MySqlCommand command = new MySqlCommand(query, connection);
                credit = Convert.ToInt32(command.ExecuteScalar());
                MySqlDataReader reader = command.ExecuteReader();
                reader.Read();
                Console.WriteLine(reader[0]);
                if ((Convert.ToInt32(reader[0]) >= 0))
                {
                    connection.Close();
                    connection.Open();
                    query = $"UPDATE accounts SET CreditStored = '" + credit + "' + '" + Convert.ToInt32(tbDeposit.Text) + "' WHERE Email = '" + email + "';";
                    command = new MySqlCommand(query, connection);
                    command.ExecuteNonQuery(); 
                    //inserting into Deposits table
                    connection.Close();
                    connection.Open();

                    var query2 = $"INSERT INTO `deposits`( `AccountEmail`, `StartTime`,`EndTime`, `Amount`) " +
                        $"VALUES ('" + email + "','" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + "','" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + "','" + Convert.ToInt32(tbDeposit.Text) + "')";
                    MySqlCommand command2 = new MySqlCommand(query2, connection);
                    command2.ExecuteNonQuery();
                    //showing a message with the new balance below
                    connection.Close();
                    connection.Open();
                    string query1 = $"SELECT CreditStored FROM accounts WHERE Email ='" + email + "';";
                    MySqlCommand command1 = new MySqlCommand(query1, connection);
                    int newCredit = Convert.ToInt32(command1.ExecuteScalar());

                    MessageBox.Show("Current credit is: " + newCredit);
                }
            }

            catch (PhidgetException ex)
            {
                lblRFIDstatus.Text = "Could not connect to the RFID-Reader!";
            }
            catch (Exception ex1)
            {
                MessageBox.Show(ex1.ToString());
            }
            finally
            {
                connection.Close();
            }
        }
        //groupbox3 e smotano
        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {
            text += '2';
            tbDeposit.Text = text;
        }

        private void bt1_Click(object sender, EventArgs e)
        {
            text += '1';
            tbDeposit.Text = text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            text += '0';
            tbDeposit.Text = text;
        }

        private void btnDeposit5_Click(object sender, EventArgs e)
        {
            try
            {
                connection.Open(); //new column named Paid For Ticket , otherwise doesn't  make any sense
                string query = $"SELECT CreditStored FROM accounts WHERE Email ='" + email + "';";
                MySqlCommand command = new MySqlCommand(query, connection);
                credit = Convert.ToInt32(command.ExecuteScalar());
                MySqlDataReader reader = command.ExecuteReader();
                reader.Read();
                Console.WriteLine(reader[0]);
                if ((Convert.ToInt32(reader[0]) >= 0))
                {
                    connection.Close();
                    connection.Open();
                    query = $"UPDATE accounts SET CreditStored = '" + credit + "' + 5 WHERE Email = '" + email + "';";
                    command = new MySqlCommand(query, connection);
                    command.ExecuteNonQuery(); //tuk e non query ,a na drugite e s execute reader , razlika?
                    //inserting into Deposits table
                    connection.Close();
                    connection.Open();
                    
                    var query2 = $"INSERT INTO `deposits`( `AccountEmail`, `StartTime`,`EndTime`, `Amount`) " +
                        $"VALUES ('" + email + "','" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + "','" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + "','5')";
                    MySqlCommand command2 = new MySqlCommand(query2, connection);
                    command2.ExecuteNonQuery();
                    //showing a message with the new balance below
                    connection.Close();
                    connection.Open();
                    string query1 = $"SELECT CreditStored FROM accounts WHERE Email ='" + email + "';";
                    MySqlCommand command1 = new MySqlCommand(query1, connection);
                    int newCredit = Convert.ToInt32(command1.ExecuteScalar());
                    
                    MessageBox.Show("Current credit is: " + newCredit);
                }
            }

            catch (PhidgetException ex)
            {
                lblRFIDstatus.Text = "Could not connect to the RFID-Reader!";
            }
            catch (Exception ex1)
            {
                MessageBox.Show(ex1.ToString());
            }
            finally
            {
                connection.Close();
            }
        }

        private void btnDeposit10_Click(object sender, EventArgs e)
        {
            try
            {
                connection.Open(); //new column named Paid For Ticket , otherwise doesn't  make any sense
                string query = $"SELECT CreditStored FROM accounts WHERE Email ='" + email + "';";
                MySqlCommand command = new MySqlCommand(query, connection);
                credit = Convert.ToInt32(command.ExecuteScalar());
                MySqlDataReader reader = command.ExecuteReader();
                reader.Read();
                Console.WriteLine(reader[0]);
                if ((Convert.ToInt32(reader[0]) >= 0))
                {
                    connection.Close();
                    connection.Open();
                    query = $"UPDATE accounts SET CreditStored = '" + credit + "' + 10 WHERE Email = '" + email + "';";
                    command = new MySqlCommand(query, connection);
                    command.ExecuteNonQuery();
                    //inserting into Deposits table
                    connection.Close();
                    connection.Open();

                    var query2 = $"INSERT INTO `deposits`(`AccountEmail`, `StartTime`,`EndTime`, `Amount`) " +
                        $"VALUES ('" + email + "','" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + "','" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + "','10')";
                    MySqlCommand command2 = new MySqlCommand(query2, connection);
                    command2.ExecuteNonQuery();
                    //showing a message with the new balance below
                    connection.Close();
                    connection.Open();
                    string query1 = $"SELECT CreditStored FROM accounts WHERE Email ='" + email + "';";
                    MySqlCommand command1 = new MySqlCommand(query1, connection);
                    int newCredit = Convert.ToInt32(command1.ExecuteScalar());
                    
                    MessageBox.Show("Current credit is: " + newCredit);
                }
            }

            catch (PhidgetException ex)
            {
                lblRFIDstatus.Text = "Could not connect to the RFID-Reader!";
            }
            catch (Exception ex1)
            {
                MessageBox.Show(ex1.ToString());
            }
            finally
            {
                connection.Close();
            }
        }

        private void btnDeposit20_Click(object sender, EventArgs e)
        {
            try
            {
                connection.Open(); //new column named Paid For Ticket , otherwise doesn't  make any sense
                string query = $"SELECT CreditStored FROM accounts WHERE Email ='" + email + "';";
                MySqlCommand command = new MySqlCommand(query, connection);
                credit = Convert.ToInt32(command.ExecuteScalar());
                MySqlDataReader reader = command.ExecuteReader();
                reader.Read();
                Console.WriteLine(reader[0]);
                if ((Convert.ToInt32(reader[0]) >= 0))
                {
                    connection.Close();
                    connection.Open();
                    query = $"UPDATE accounts SET CreditStored = '" + credit + "' + 20 WHERE Email = '" + email + "';";
                    command = new MySqlCommand(query, connection);
                    command.ExecuteNonQuery();
                    //inserting into Deposits table
                    connection.Close();
                    connection.Open();

                    var query2 = $"INSERT INTO `deposits`(`AccountEmail`, `StartTime`,`EndTime`, `Amount`) " +
                        $"VALUES ('" + email + "','" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + "','" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + "','20')";
                    MySqlCommand command2 = new MySqlCommand(query2, connection);
                    command2.ExecuteNonQuery();
                    //showing a message with the new balance below
                    connection.Close();
                    connection.Open();
                    string query1 = $"SELECT CreditStored FROM accounts WHERE Email ='" + email + "';";
                    MySqlCommand command1 = new MySqlCommand(query1, connection);
                    int newCredit = Convert.ToInt32(command1.ExecuteScalar());
                    
                    MessageBox.Show("Current credit is: " + newCredit);
                }
            }

            catch (PhidgetException ex)
            {
                lblRFIDstatus.Text = "Could not connect to the RFID-Reader!";
            }
            catch (Exception ex1)
            {
                MessageBox.Show(ex1.ToString());
            }
            finally
            {
                connection.Close();
            }
        }

        private void btnDeposit50_Click(object sender, EventArgs e)
        {
            try
            {
                connection.Open(); //new column named Paid For Ticket , otherwise doesn't  make any sense
                string query = $"SELECT CreditStored FROM accounts WHERE Email ='" + email + "';";
                MySqlCommand command = new MySqlCommand(query, connection);
                credit = Convert.ToInt32(command.ExecuteScalar());
                MySqlDataReader reader = command.ExecuteReader();
                reader.Read();
                Console.WriteLine(reader[0]);
                if ((Convert.ToInt32(reader[0]) >= 0))
                {
                    connection.Close();
                    connection.Open();
                    query = $"UPDATE accounts SET CreditStored = '" + credit + "' + 50 WHERE Email = '" + email + "';";
                    command = new MySqlCommand(query, connection);
                    command.ExecuteNonQuery();
                    //inserting into Deposits table
                    connection.Close();
                    connection.Open();

                    var query2 = $"INSERT INTO `deposits`(`AccountEmail`, `StartTime`, `EndTime`, `Amount`) " +
                        $"VALUES ('" + email + "','" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + "','" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + "','50')";
                    MySqlCommand command2 = new MySqlCommand(query2, connection);
                    command2.ExecuteNonQuery();
                    //showing a message with the new balance below
                    connection.Close();
                    connection.Open();
                    string query1 = $"SELECT CreditStored FROM accounts WHERE Email ='" + email + "';";
                    MySqlCommand command1 = new MySqlCommand(query1, connection);
                    int newCredit = Convert.ToInt32(command1.ExecuteScalar());
        
                    MessageBox.Show("Current credit is: " + newCredit);
                }
            }

            catch (PhidgetException ex)
            {
                lblRFIDstatus.Text = "Could not connect to the RFID-Reader!";
            }
            catch (Exception ex1)
            {
                MessageBox.Show(ex1.ToString());
            }
            finally
            {
                connection.Close();
            }
        }

        private void bt7_Click(object sender, EventArgs e)
        {
            text += '7';
            tbDeposit.Text = text;
        }

        private void bt8_Click(object sender, EventArgs e)
        {
            text += '8';
            tbDeposit.Text = text;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            text += '9';
            tbDeposit.Text = text;
        }

        private void bt4_Click(object sender, EventArgs e)
        {
            text += '4';
            tbDeposit.Text = text;
        }

        private void bt5_Click(object sender, EventArgs e)
        {
            text += '5';
            tbDeposit.Text = text;
        }

        private void bt6_Click(object sender, EventArgs e)
        {
            text += '6';
            tbDeposit.Text = text;
        }

        private void bt3_Click(object sender, EventArgs e)
        {
            text += '3';
            tbDeposit.Text = text;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            text = "";
            tbDeposit.Text = text;
        }

        private void bt10_FormClosed(object sender, FormClosedEventArgs e)
        {
            connection.Close();
            myRFIDReader.Close();
        }
    }
}
