﻿namespace collectiveForm
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOpenAdmin = new System.Windows.Forms.Button();
            this.btnOpenATM = new System.Windows.Forms.Button();
            this.btnOpenBuyItem = new System.Windows.Forms.Button();
            this.btnOpenCamping = new System.Windows.Forms.Button();
            this.btnOpenEntrance = new System.Windows.Forms.Button();
            this.btnOpenInfoDesk = new System.Windows.Forms.Button();
            this.btnOpenRentItem = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnOpenAdmin
            // 
            this.btnOpenAdmin.Location = new System.Drawing.Point(12, 12);
            this.btnOpenAdmin.Name = "btnOpenAdmin";
            this.btnOpenAdmin.Size = new System.Drawing.Size(189, 23);
            this.btnOpenAdmin.TabIndex = 0;
            this.btnOpenAdmin.Text = "Open Admin";
            this.btnOpenAdmin.UseVisualStyleBackColor = true;
            this.btnOpenAdmin.Click += new System.EventHandler(this.btnOpenAdmin_Click);
            // 
            // btnOpenATM
            // 
            this.btnOpenATM.Location = new System.Drawing.Point(12, 39);
            this.btnOpenATM.Name = "btnOpenATM";
            this.btnOpenATM.Size = new System.Drawing.Size(189, 23);
            this.btnOpenATM.TabIndex = 1;
            this.btnOpenATM.Text = "Open ATMLog";
            this.btnOpenATM.UseVisualStyleBackColor = true;
            this.btnOpenATM.Click += new System.EventHandler(this.btnOpenATM_Click);
            // 
            // btnOpenBuyItem
            // 
            this.btnOpenBuyItem.Enabled = false;
            this.btnOpenBuyItem.Location = new System.Drawing.Point(12, 68);
            this.btnOpenBuyItem.Name = "btnOpenBuyItem";
            this.btnOpenBuyItem.Size = new System.Drawing.Size(189, 23);
            this.btnOpenBuyItem.TabIndex = 2;
            this.btnOpenBuyItem.Text = "Open BuyItem";
            this.btnOpenBuyItem.UseVisualStyleBackColor = true;
            this.btnOpenBuyItem.Click += new System.EventHandler(this.btnOpenBuyItem_Click);
            // 
            // btnOpenCamping
            // 
            this.btnOpenCamping.Location = new System.Drawing.Point(12, 97);
            this.btnOpenCamping.Name = "btnOpenCamping";
            this.btnOpenCamping.Size = new System.Drawing.Size(189, 23);
            this.btnOpenCamping.TabIndex = 3;
            this.btnOpenCamping.Text = "Open Camping Gate";
            this.btnOpenCamping.UseVisualStyleBackColor = true;
            this.btnOpenCamping.Click += new System.EventHandler(this.btnOpenCamping_Click);
            // 
            // btnOpenEntrance
            // 
            this.btnOpenEntrance.Location = new System.Drawing.Point(12, 126);
            this.btnOpenEntrance.Name = "btnOpenEntrance";
            this.btnOpenEntrance.Size = new System.Drawing.Size(189, 23);
            this.btnOpenEntrance.TabIndex = 4;
            this.btnOpenEntrance.Text = "Open Entrance Gate";
            this.btnOpenEntrance.UseVisualStyleBackColor = true;
            this.btnOpenEntrance.Click += new System.EventHandler(this.btnOpenEntrance_Click);
            // 
            // btnOpenInfoDesk
            // 
            this.btnOpenInfoDesk.Location = new System.Drawing.Point(12, 155);
            this.btnOpenInfoDesk.Name = "btnOpenInfoDesk";
            this.btnOpenInfoDesk.Size = new System.Drawing.Size(189, 23);
            this.btnOpenInfoDesk.TabIndex = 5;
            this.btnOpenInfoDesk.Text = "Open InfoDesk";
            this.btnOpenInfoDesk.UseVisualStyleBackColor = true;
            this.btnOpenInfoDesk.Click += new System.EventHandler(this.btnOpenInfoDesk_Click);
            // 
            // btnOpenRentItem
            // 
            this.btnOpenRentItem.Location = new System.Drawing.Point(12, 184);
            this.btnOpenRentItem.Name = "btnOpenRentItem";
            this.btnOpenRentItem.Size = new System.Drawing.Size(189, 23);
            this.btnOpenRentItem.TabIndex = 6;
            this.btnOpenRentItem.Text = "Open RentItem";
            this.btnOpenRentItem.UseVisualStyleBackColor = true;
            this.btnOpenRentItem.Click += new System.EventHandler(this.btnOpenRentItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(211, 217);
            this.Controls.Add(this.btnOpenRentItem);
            this.Controls.Add(this.btnOpenInfoDesk);
            this.Controls.Add(this.btnOpenEntrance);
            this.Controls.Add(this.btnOpenCamping);
            this.Controls.Add(this.btnOpenBuyItem);
            this.Controls.Add(this.btnOpenATM);
            this.Controls.Add(this.btnOpenAdmin);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOpenAdmin;
        private System.Windows.Forms.Button btnOpenATM;
        private System.Windows.Forms.Button btnOpenBuyItem;
        private System.Windows.Forms.Button btnOpenCamping;
        private System.Windows.Forms.Button btnOpenEntrance;
        private System.Windows.Forms.Button btnOpenInfoDesk;
        private System.Windows.Forms.Button btnOpenRentItem;
    }
}

