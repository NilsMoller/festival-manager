﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Phidget22;
using Phidget22.Events;
using MySql.Data.MySqlClient;
using MySql.Data;

namespace Camping_Gate
{
    public partial class Form1 : Form
    {
        RFID rfid;
        private Camping testSubjects = new Camping();
        DBCamping dc;
        string id;
        public Form1()
        {
            InitializeComponent();
        }

        public void StartUp()
        {
            try
            {
                rfid = new RFID();

                rfid.Attach += rfid_Attach;
                rfid.Detach += rfid_Detach;
                rfid.Error += rfid_Error;
                rfid.Tag += rfid_Tag;
                rfid.TagLost += rfid_TagLost;

                rfid.Open();
            }
            catch (PhidgetException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
            catch
            {
                MessageBox.Show("something else");
            }
        }

        private void rfid_Attach(object sender, AttachEventArgs e)
        {
            lblStatus.Text = "Ready";
        }

        private void rfid_Detach(object sender, DetachEventArgs e)
        {
            lblStatus.Text = "Detach";
        }

        private void rfid_Error(object sender, ErrorEventArgs e)
        {
            MessageBox.Show("error");
        }

        private void rfid_Tag(object sender, RFIDTagEventArgs e)
        {
             try
            {
                if (SearchPerson(e.Tag) == null)
                {
                    lblStatus.Text = "Account\nUnregistered";
                    tbID.Text = e.Tag;
                }
                else
                {
                    Console.WriteLine(SearchPerson(e.Tag).Ticket);
                    tbaccountidcode.Text = SearchPerson(e.Tag).AccountId;
                    //Console.WriteLine(SearchPerson(e.Tag).AccountId);
                    tbaccountname.Text = SearchPerson(e.Tag).Name;
                    tbID.Text = SearchPerson(e.Tag).IdCard;

                    WithCamping test = dc.GetCamping(SearchPerson(e.Tag));
                    if (SearchPerson(e.Tag).Ticket == "-1" || SearchPerson(e.Tag).Ticket == null || SearchPerson(e.Tag).Ticket == "")
                    {
                        tbcampingno.Text = "No Camping";
                        pnlID.BackColor = Color.Red;
                        lblStatus.Text = "No Camping";
                    }
                    else if (test.CampingNo > 0)
                    {
                        tbcampingno.Text = test.CampingNo.ToString();
                        pnlID.BackColor = Color.Lime;
                        lblStatus.Text = "Enter";
                        dc.UpdateAccount(SearchPerson(e.Tag).IdCard);
                    }
                    else
                    {
                        tbcampingno.Text = "No Camping";
                        pnlID.BackColor = Color.Red;
                        lblStatus.Text = "No Camping";
                    }
                }
            }
            catch (Exception)
            {

            }
            
        }
            private Customer SearchPerson(string id)
            {
                List<Customer> allPeople = dc.GetAllPeople();

                foreach (Customer p in allPeople)
                {
                    if (p.getId() == id)
                    { return p; }
                }
                return null;
            }

        private void rfid_TagLost(object sender, RFIDTagLostEventArgs e)
        {
            lblStatus.Text = "Next ID";
            tbaccountidcode.Clear();
            tbaccountname.Clear();
            tbID.Clear();
            tbcampingno.Clear();
            pnlID.BackColor = Color.MediumAquamarine;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            dc.connection.Close();
            rfid.Close();
        }

        private void btnConnection_Click(object sender, EventArgs e)
        {
            dc = new DBCamping();
            StartUp();
        }
    }
}
