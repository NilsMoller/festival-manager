﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;
using Phidget22;
using Phidget22.Events;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Security.Cryptography;



namespace Entrance_Gate
{
    public partial class Form1 : Form
    {

        MySqlConnection connection = new MySqlConnection("server=studmysql01.fhict.local;" +
                                                          "database=dbi380048;" +
                                                          "user id=dbi380048;" +
                                                          "password=pcsb;");
        private RFID myRFIDReader;
        private Color controlBackColor;
        private string connectedTag;
        string email;
        public Form1()
        {
            InitializeComponent();
            try
            {
                myRFIDReader = new RFID();
                myRFIDReader.Attach += MyRFIDReader_Attach;
                myRFIDReader.Detach += MyRFIDReader_Detach;
                myRFIDReader.Tag += MyRFIDReader_Tag;
                myRFIDReader.TagLost += MyRFIDReader_TagLost;
            }
            catch (PhidgetException ex)
            {
                lblRFIDstatus.Text = "Could not startup!";
            }
        }

        private void MyRFIDReader_TagLost(object sender, RFIDTagLostEventArgs e)
        {
            this.panel2.BackColor = controlBackColor;
            lblRFIDstatus.Text = "No tag...";
        }

        private void MyRFIDReader_Tag(object sender, RFIDTagEventArgs e)
        {
            this.panel2.BackColor = Color.Green;
            lblRFIDstatus.Text = $"Tag: {e.Tag} found!";
            connectedTag = e.Tag;
            connection.Open();
            string query = $"SELECT  Email  FROM accounts WHERE RFID =  '" + connectedTag + "' ;";

            MySqlCommand command = new MySqlCommand(query, connection);
            //MySqlDataReader reader = command.ExecuteReader();
            //var check = reader.Read();        
            try
            {
                email = (string)command.ExecuteScalar();
                tbIDNumber.Text = email;
            }
            finally
            {
                connection.Close();
            }

        }

        private void MyRFIDReader_Detach(object sender, DetachEventArgs e)
        {
            lblRFIDstatus.Text = "Detached RFID-Readerrr.";
        }

        private void MyRFIDReader_Attach(object sender, AttachEventArgs e)
        {
            lblRFIDstatus.Text = "Attached RFID-Reader.";
        }
        //check out button
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (tbIDNumber.Text != "")
                {
                    //When a row in LOANS is added , uncomment here and after the second connection use command1 query1 reader 1 to check out
                    //check if the visitor has any unreturned products
                    connection.Open();
                    string query = $"SELECT * FROM loans WHERE AccountEmail = '" + email + "'";
                    MySqlCommand command = new MySqlCommand(query, connection);
                    MySqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        MessageBox.Show("That person has not returned all his loaned items! Please return those before checking out.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else if(!reader.HasRows)//no debts
                    {
                        connection.Close();

                        connection.Open();
                        string query1 = $"SELECT CheckedInEvent FROM accounts WHERE Email = '" + email + "';";
                        MySqlCommand command1 = new MySqlCommand(query1, connection);
                        MySqlDataReader reader1 = command1.ExecuteReader();
                        reader1.Read();
                        //Console.WriteLine(reader1[0]);
                        if ((bool)reader1[0] == true) //ne bachka
                        {
                            connection.Close();
                            connection.Open();
                            query1 = $"UPDATE accounts SET CheckedInEvent = 0 WHERE Email = '" + email + "';";
                            command1 = new MySqlCommand(query1, connection);
                            int throwaway = command1.ExecuteNonQuery();
                            connection.Close();
                            connection.Open();

                            query1 = $"SELECT Name, Email FROM accounts WHERE Email = '" + email + "';";
                            command1 = new MySqlCommand(query1, connection);
                            reader1 = command1.ExecuteReader();
                            reader1.Read();

                            lboxVisitors.Items.Add("Checking Out:");
                            lboxVisitors.Items.Add($"{reader1[0]} with email: {reader1[1]} ");
                            lboxVisitors.Items.Add("---------------------------------------");
                        }
                    }
                    else
                    {
                        MessageBox.Show("This person is not checked in, hence he cant check out!");
                    }

                }
                else
                {
                    MessageBox.Show("Please insert a valid account ID", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (InvalidOperationException)
            {
                MessageBox.Show("Error, please try again.");
                lboxVisitors.Items.Add("Invalid account ID entered");
                lboxVisitors.Items.Add("---------------------------------------");
            }
            catch (MySqlException)
            {
                MessageBox.Show("Please enter an existing ID to check out", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                lboxVisitors.Items.Add("Invalid account ID entered");
                lboxVisitors.Items.Add("---------------------------------------");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            finally
            {
                connection.Close();

                //list box scrolls down if there are more entries than its height
                lboxVisitors.TopIndex = lboxVisitors.Items.Count - 1;
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        #region Checking in and out
        private void btnCheckIn_Click(object sender, EventArgs e)
        {
            try
            {
                //if (int.TryParse(tbIDNumber.Text, out int result))
                if (tbIDNumber.Text != "")
                {

                    connection.Open(); //new column named Paid For Ticket , otherwise doesn't  make any sense
                    string query = $"SELECT CheckedInEvent,  Name , PaidForTicket FROM accounts WHERE Email ='" + email + "';";
                    MySqlCommand command = new MySqlCommand(query, connection);
                    MySqlDataReader reader = command.ExecuteReader();
                    reader.Read();
                    Console.WriteLine(reader[0]);
                    if (Convert.ToInt32(reader[2]) == 0)
                    {
                        MessageBox.Show($"{reader[1].ToString()}  has not paid for a ticket!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        lboxVisitors.Items.Add($"{reader[1]} {reader[2]} does not have a ticket");
                        lboxVisitors.Items.Add("---------------------------------------");
                        reader.Close();
                        MessageBox.Show("To buy a ticket, please visit the Info Desk.");

                    }
                    else if (Convert.ToInt32(reader[2]) == 1)
                    {
                        connection.Close();
                        connection.Open();
                        query = $"UPDATE accounts SET CheckedInEvent = 1 WHERE Email = '" + email + "';";
                        command = new MySqlCommand(query, connection);
                        int throwaway = command.ExecuteNonQuery();

                        query = $"SELECT Name FROM accounts WHERE Email = '" + email + "';";
                        command = new MySqlCommand(query, connection);
                        reader = command.ExecuteReader();
                        reader.Read();

                        lboxVisitors.Items.Add("Checked In:");
                        lboxVisitors.Items.Add($"{reader[0]} , with email:'" + email + "'");
                        lboxVisitors.Items.Add("---------------------------------------");
                    }
                }
                else
                {
                    MessageBox.Show("Please insert a valid account ID", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (InvalidOperationException)
            {
                MessageBox.Show("An error occurred, please try again.");
                lboxVisitors.Items.Add("Invalid account ID entered");
                lboxVisitors.Items.Add("---------------------------------------");
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                lboxVisitors.Items.Add("Invalid account ID entered");
                lboxVisitors.Items.Add("---------------------------------------");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            finally
            {
                connection.Close();
                //list box scrolls down if there are more entries than its height
                lboxVisitors.TopIndex = lboxVisitors.Items.Count - 1;
            }
        }
        #endregion

        private void btnConnect_Click(object sender, EventArgs e)
        {
            A();
            B();
        }
        /*
        private void btnSetUpRFIDID_Click(object sender, EventArgs e)
        {
            connection.Open();
            string query = $"SELECT  Email  FROM accounts WHERE RFID =  '" + connectedTag + "' ;";

            MySqlCommand command = new MySqlCommand(query, connection);
            //MySqlDataReader reader = command.ExecuteReader();
            //var check = reader.Read();        
            try
            {
                email = (string)command.ExecuteScalar();
                tbIDNumber.Text = email;
            }
            finally
            {
                connection.Close();
            }
        }
        */
        

        private void A()
        {
            try
            {
                myRFIDReader.Open();

            }
            catch (PhidgetException ex)
            {
                lblRFIDstatus.Text = "Could not connect to the RFID-Reader!";
            }
        }

        private void B()
        {
            connection.Open();
            string query = $"SELECT  Email  FROM accounts WHERE RFID =  '" + connectedTag + "' ;";

            MySqlCommand command = new MySqlCommand(query, connection);
            //MySqlDataReader reader = command.ExecuteReader();
            //var check = reader.Read();        
            try
            {
                email = (string)command.ExecuteScalar();
                tbIDNumber.Text = email;
            }
            finally
            {
                connection.Close();
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            myRFIDReader.Close();
            connection.Close();
        }

        private void lblRFIDstatus_Click(object sender, EventArgs e)
        {

        }
    }
}