﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Model
{
    public class Loan
    {
        public int Id { get; set; }
        public int ItemId { get; set; }
        public Item ItemName { get; set; }
        public string AccountEmail { get; set; }
        [ForeignKey("AccountEmail")]
        public Account Account { get; set; }
        public bool IsReturned { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }
}
