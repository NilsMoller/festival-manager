﻿namespace InfoDesk
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPay = new System.Windows.Forms.Button();
            this.lblTotalPrice = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.tbAccountRFID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbAccountName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbNumberPeople = new System.Windows.Forms.ComboBox();
            this.rbTicketNo = new System.Windows.Forms.RadioButton();
            this.tbAssignTicket = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.rbTicketYes = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.gbCampRequest = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cbCampSpot = new System.Windows.Forms.ComboBox();
            this.lblCamping = new System.Windows.Forms.Label();
            this.rbCampNo = new System.Windows.Forms.RadioButton();
            this.rbCampYes = new System.Windows.Forms.RadioButton();
            this.tbCampSpot = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbCampRequest.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPay
            // 
            this.btnPay.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPay.Location = new System.Drawing.Point(55, 266);
            this.btnPay.Name = "btnPay";
            this.btnPay.Size = new System.Drawing.Size(167, 67);
            this.btnPay.TabIndex = 29;
            this.btnPay.Text = "Pay";
            this.btnPay.UseVisualStyleBackColor = true;
            this.btnPay.Click += new System.EventHandler(this.btnPay_Click);
            // 
            // lblTotalPrice
            // 
            this.lblTotalPrice.AutoSize = true;
            this.lblTotalPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalPrice.Location = new System.Drawing.Point(156, 235);
            this.lblTotalPrice.Name = "lblTotalPrice";
            this.lblTotalPrice.Size = new System.Drawing.Size(66, 22);
            this.lblTotalPrice.TabIndex = 28;
            this.lblTotalPrice.Text = "€ 0.00";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(52, 236);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 18);
            this.label11.TabIndex = 26;
            this.label11.Text = "Total Price:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(50, 194);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(116, 25);
            this.label10.TabIndex = 25;
            this.label10.Text = "PAYMENT";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.LightCyan;
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.tbAccountRFID);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.tbAccountName);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(31, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(383, 157);
            this.panel2.TabIndex = 24;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(65, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(276, 25);
            this.label8.TabIndex = 4;
            this.label8.Text = "ACCOUNT INFORMATION";
            // 
            // tbAccountRFID
            // 
            this.tbAccountRFID.Enabled = false;
            this.tbAccountRFID.Location = new System.Drawing.Point(151, 109);
            this.tbAccountRFID.Name = "tbAccountRFID";
            this.tbAccountRFID.ReadOnly = true;
            this.tbAccountRFID.Size = new System.Drawing.Size(211, 20);
            this.tbAccountRFID.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(21, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "AccountID Code:";
            // 
            // tbAccountName
            // 
            this.tbAccountName.Enabled = false;
            this.tbAccountName.Location = new System.Drawing.Point(151, 65);
            this.tbAccountName.Name = "tbAccountName";
            this.tbAccountName.ReadOnly = true;
            this.tbAccountName.Size = new System.Drawing.Size(211, 20);
            this.tbAccountName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Account Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 18);
            this.label5.TabIndex = 0;
            this.label5.Text = "Buy Ticket:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(95, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(168, 25);
            this.label3.TabIndex = 4;
            this.label3.Text = "Ticket Purchase";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.PaleGreen;
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.gbCampRequest);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(420, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(338, 349);
            this.panel1.TabIndex = 30;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Lavender;
            this.groupBox1.Controls.Add(this.cbNumberPeople);
            this.groupBox1.Controls.Add(this.rbTicketNo);
            this.groupBox1.Controls.Add(this.tbAssignTicket);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.rbTicketYes);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(24, 54);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(299, 118);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ticket Request";
            // 
            // cbNumberPeople
            // 
            this.cbNumberPeople.FormattingEnabled = true;
            this.cbNumberPeople.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6"});
            this.cbNumberPeople.Location = new System.Drawing.Point(154, 85);
            this.cbNumberPeople.Name = "cbNumberPeople";
            this.cbNumberPeople.Size = new System.Drawing.Size(136, 21);
            this.cbNumberPeople.TabIndex = 12;
            this.cbNumberPeople.Text = "1";
            this.cbNumberPeople.SelectedIndexChanged += new System.EventHandler(this.cbNumberPeople_SelectedIndexChanged);
            // 
            // rbTicketNo
            // 
            this.rbTicketNo.AutoSize = true;
            this.rbTicketNo.Checked = true;
            this.rbTicketNo.Location = new System.Drawing.Point(218, 30);
            this.rbTicketNo.Name = "rbTicketNo";
            this.rbTicketNo.Size = new System.Drawing.Size(39, 17);
            this.rbTicketNo.TabIndex = 10;
            this.rbTicketNo.TabStop = true;
            this.rbTicketNo.Text = "No";
            this.rbTicketNo.UseVisualStyleBackColor = true;
            this.rbTicketNo.CheckedChanged += new System.EventHandler(this.rbTicketNo_CheckedChanged);
            // 
            // tbAssignTicket
            // 
            this.tbAssignTicket.Enabled = false;
            this.tbAssignTicket.Location = new System.Drawing.Point(154, 58);
            this.tbAssignTicket.Name = "tbAssignTicket";
            this.tbAssignTicket.ReadOnly = true;
            this.tbAssignTicket.Size = new System.Drawing.Size(136, 20);
            this.tbAssignTicket.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 18);
            this.label6.TabIndex = 8;
            this.label6.Text = "Assigned ticket:";
            // 
            // rbTicketYes
            // 
            this.rbTicketYes.AutoSize = true;
            this.rbTicketYes.Location = new System.Drawing.Point(154, 30);
            this.rbTicketYes.Name = "rbTicketYes";
            this.rbTicketYes.Size = new System.Drawing.Size(43, 17);
            this.rbTicketYes.TabIndex = 7;
            this.rbTicketYes.Text = "Yes";
            this.rbTicketYes.UseVisualStyleBackColor = true;
            this.rbTicketYes.CheckedChanged += new System.EventHandler(this.rbTicketYes_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(7, 84);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(132, 18);
            this.label7.TabIndex = 4;
            this.label7.Text = "Number of People:";
            // 
            // gbCampRequest
            // 
            this.gbCampRequest.BackColor = System.Drawing.Color.Lavender;
            this.gbCampRequest.Controls.Add(this.label9);
            this.gbCampRequest.Controls.Add(this.cbCampSpot);
            this.gbCampRequest.Controls.Add(this.lblCamping);
            this.gbCampRequest.Controls.Add(this.rbCampNo);
            this.gbCampRequest.Controls.Add(this.rbCampYes);
            this.gbCampRequest.Controls.Add(this.tbCampSpot);
            this.gbCampRequest.Controls.Add(this.label4);
            this.gbCampRequest.Location = new System.Drawing.Point(24, 206);
            this.gbCampRequest.Name = "gbCampRequest";
            this.gbCampRequest.Size = new System.Drawing.Size(299, 128);
            this.gbCampRequest.TabIndex = 5;
            this.gbCampRequest.TabStop = false;
            this.gbCampRequest.Text = "Camping Request";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(7, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 18);
            this.label9.TabIndex = 13;
            this.label9.Text = "Current Spot:";
            // 
            // cbCampSpot
            // 
            this.cbCampSpot.Enabled = false;
            this.cbCampSpot.FormattingEnabled = true;
            this.cbCampSpot.Location = new System.Drawing.Point(143, 85);
            this.cbCampSpot.Name = "cbCampSpot";
            this.cbCampSpot.Size = new System.Drawing.Size(136, 21);
            this.cbCampSpot.TabIndex = 11;
            // 
            // lblCamping
            // 
            this.lblCamping.AutoSize = true;
            this.lblCamping.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCamping.Location = new System.Drawing.Point(7, 28);
            this.lblCamping.Name = "lblCamping";
            this.lblCamping.Size = new System.Drawing.Size(134, 18);
            this.lblCamping.TabIndex = 12;
            this.lblCamping.Text = "Get Camping Spot:";
            // 
            // rbCampNo
            // 
            this.rbCampNo.AutoSize = true;
            this.rbCampNo.Checked = true;
            this.rbCampNo.Location = new System.Drawing.Point(218, 29);
            this.rbCampNo.Name = "rbCampNo";
            this.rbCampNo.Size = new System.Drawing.Size(39, 17);
            this.rbCampNo.TabIndex = 11;
            this.rbCampNo.TabStop = true;
            this.rbCampNo.Text = "No";
            this.rbCampNo.UseVisualStyleBackColor = true;
            this.rbCampNo.CheckedChanged += new System.EventHandler(this.rbCampNo_CheckedChanged);
            // 
            // rbCampYes
            // 
            this.rbCampYes.AutoSize = true;
            this.rbCampYes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbCampYes.Location = new System.Drawing.Point(154, 27);
            this.rbCampYes.Name = "rbCampYes";
            this.rbCampYes.Size = new System.Drawing.Size(45, 19);
            this.rbCampYes.TabIndex = 6;
            this.rbCampYes.Text = "Yes";
            this.rbCampYes.UseVisualStyleBackColor = true;
            this.rbCampYes.CheckedChanged += new System.EventHandler(this.rbCampYes_CheckedChanged);
            // 
            // tbCampSpot
            // 
            this.tbCampSpot.Enabled = false;
            this.tbCampSpot.Location = new System.Drawing.Point(144, 55);
            this.tbCampSpot.Name = "tbCampSpot";
            this.tbCampSpot.ReadOnly = true;
            this.tbCampSpot.Size = new System.Drawing.Size(136, 20);
            this.tbCampSpot.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 18);
            this.label4.TabIndex = 6;
            this.label4.Text = "Spot Assigned:";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(240, 266);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(103, 67);
            this.btnCancel.TabIndex = 31;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 373);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnPay);
            this.Controls.Add(this.lblTotalPrice);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.panel2);
            this.Name = "Form1";
            this.Text = "Info Desk";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbCampRequest.ResumeLayout(false);
            this.gbCampRequest.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnPay;
        private System.Windows.Forms.Label lblTotalPrice;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbAccountRFID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbAccountName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rbCampYes;
        private System.Windows.Forms.GroupBox gbCampRequest;
        private System.Windows.Forms.TextBox tbCampSpot;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton rbTicketYes;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox tbAssignTicket;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbTicketNo;
        private System.Windows.Forms.RadioButton rbCampNo;
        private System.Windows.Forms.Label lblCamping;
        private System.Windows.Forms.ComboBox cbCampSpot;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbNumberPeople;
    }
}

