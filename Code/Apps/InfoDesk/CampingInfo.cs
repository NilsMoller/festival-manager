﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfoDesk
{
    class CampingInfo
    {
        private int campingNo;
        private int available;
        
        public int CampingNo
        {
            get { return campingNo; }
            set { campingNo = value; }
        }
        public int Available
        {
            get { return available; }
            set { available = value; }
        }

        public CampingInfo(int camping, int available)
        {
            this.campingNo = camping;
            this.available = available;
        }
    }
}
