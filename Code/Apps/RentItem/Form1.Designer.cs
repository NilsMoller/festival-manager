﻿namespace RentItem
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.tbaccountemail = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbaccountid = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btrfidopen = new System.Windows.Forms.Button();
            this.lbrfid = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.lbshowreturnlist = new System.Windows.Forms.ListBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btremove = new System.Windows.Forms.Button();
            this.btreturnitems = new System.Windows.Forms.Button();
            this.btmovedown = new System.Windows.Forms.Button();
            this.lbprice = new System.Windows.Forms.Label();
            this.btmoveup = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btrent = new System.Windows.Forms.Button();
            this.cbitemname = new System.Windows.Forms.ComboBox();
            this.lbreturneditem = new System.Windows.Forms.ListBox();
            this.lbrenteditem = new System.Windows.Forms.ListBox();
            this.lbname = new System.Windows.Forms.ListBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lbrfidinfo = new System.Windows.Forms.ListBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.LightCyan;
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.tbaccountemail);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.tbaccountid);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(12, 54);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(401, 176);
            this.panel2.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(65, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(276, 25);
            this.label8.TabIndex = 4;
            this.label8.Text = "ACCOUNT INFORMATION";
            // 
            // tbaccountemail
            // 
            this.tbaccountemail.Enabled = false;
            this.tbaccountemail.Location = new System.Drawing.Point(151, 109);
            this.tbaccountemail.Name = "tbaccountemail";
            this.tbaccountemail.ReadOnly = true;
            this.tbaccountemail.Size = new System.Drawing.Size(211, 20);
            this.tbaccountemail.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(21, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Account Email:";
            // 
            // tbaccountid
            // 
            this.tbaccountid.Enabled = false;
            this.tbaccountid.Location = new System.Drawing.Point(151, 65);
            this.tbaccountid.Name = "tbaccountid";
            this.tbaccountid.ReadOnly = true;
            this.tbaccountid.Size = new System.Drawing.Size(211, 20);
            this.tbaccountid.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Account ID Code:";
            // 
            // btrfidopen
            // 
            this.btrfidopen.Enabled = false;
            this.btrfidopen.Location = new System.Drawing.Point(13, 14);
            this.btrfidopen.Name = "btrfidopen";
            this.btrfidopen.Size = new System.Drawing.Size(284, 25);
            this.btrfidopen.TabIndex = 24;
            this.btrfidopen.Text = "Open RFID";
            this.btrfidopen.UseVisualStyleBackColor = true;
            this.btrfidopen.Visible = false;
            this.btrfidopen.Click += new System.EventHandler(this.btrfidopen_Click);
            // 
            // lbrfid
            // 
            this.lbrfid.AutoSize = true;
            this.lbrfid.Location = new System.Drawing.Point(362, 20);
            this.lbrfid.Name = "lbrfid";
            this.lbrfid.Size = new System.Drawing.Size(150, 13);
            this.lbrfid.TabIndex = 25;
            this.lbrfid.Text = "RFID IS NOT CONNECTED...";
            this.lbrfid.Visible = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.MistyRose;
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.lbshowreturnlist);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.btremove);
            this.panel3.Controls.Add(this.btreturnitems);
            this.panel3.Controls.Add(this.btmovedown);
            this.panel3.Controls.Add(this.lbprice);
            this.panel3.Controls.Add(this.btmoveup);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.btrent);
            this.panel3.Controls.Add(this.cbitemname);
            this.panel3.Controls.Add(this.lbreturneditem);
            this.panel3.Controls.Add(this.lbrenteditem);
            this.panel3.Controls.Add(this.lbname);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Location = new System.Drawing.Point(462, 54);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(739, 613);
            this.panel3.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(369, 459);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 13);
            this.label4.TabIndex = 36;
            this.label4.Text = "Show your returnlist:";
            // 
            // lbshowreturnlist
            // 
            this.lbshowreturnlist.FormattingEnabled = true;
            this.lbshowreturnlist.HorizontalScrollbar = true;
            this.lbshowreturnlist.Location = new System.Drawing.Point(371, 493);
            this.lbshowreturnlist.Name = "lbshowreturnlist";
            this.lbshowreturnlist.Size = new System.Drawing.Size(325, 108);
            this.lbshowreturnlist.TabIndex = 35;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(369, 243);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(174, 13);
            this.label10.TabIndex = 34;
            this.label10.Text = "The list for items you want to return:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(369, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(176, 13);
            this.label6.TabIndex = 33;
            this.label6.Text = "The list for items you already rented:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(165, 13);
            this.label3.TabIndex = 32;
            this.label3.Text = "The list for items you want to rent:";
            // 
            // btremove
            // 
            this.btremove.Location = new System.Drawing.Point(43, 402);
            this.btremove.Name = "btremove";
            this.btremove.Size = new System.Drawing.Size(103, 29);
            this.btremove.TabIndex = 30;
            this.btremove.Text = "Remove";
            this.btremove.UseVisualStyleBackColor = true;
            this.btremove.Click += new System.EventHandler(this.btremove_Click);
            // 
            // btreturnitems
            // 
            this.btreturnitems.Location = new System.Drawing.Point(569, 402);
            this.btreturnitems.Name = "btreturnitems";
            this.btreturnitems.Size = new System.Drawing.Size(103, 29);
            this.btreturnitems.TabIndex = 29;
            this.btreturnitems.Text = "Return Items";
            this.btreturnitems.UseVisualStyleBackColor = true;
            this.btreturnitems.Click += new System.EventHandler(this.btreturnitems_Click);
            // 
            // btmovedown
            // 
            this.btmovedown.Location = new System.Drawing.Point(461, 179);
            this.btmovedown.Name = "btmovedown";
            this.btmovedown.Size = new System.Drawing.Size(125, 29);
            this.btmovedown.TabIndex = 27;
            this.btmovedown.Text = "Want to return";
            this.btmovedown.UseVisualStyleBackColor = true;
            this.btmovedown.Click += new System.EventHandler(this.btmovedown_Click);
            // 
            // lbprice
            // 
            this.lbprice.AutoSize = true;
            this.lbprice.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbprice.Location = new System.Drawing.Point(181, 546);
            this.lbprice.Name = "lbprice";
            this.lbprice.Size = new System.Drawing.Size(66, 22);
            this.lbprice.TabIndex = 22;
            this.lbprice.Text = "€ 0.00";
            // 
            // btmoveup
            // 
            this.btmoveup.Location = new System.Drawing.Point(394, 402);
            this.btmoveup.Name = "btmoveup";
            this.btmoveup.Size = new System.Drawing.Size(149, 29);
            this.btmoveup.TabIndex = 26;
            this.btmoveup.Text = "Continue renting";
            this.btmoveup.UseVisualStyleBackColor = true;
            this.btmoveup.Click += new System.EventHandler(this.btmoveup_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(40, 547);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 18);
            this.label5.TabIndex = 19;
            this.label5.Text = "Total Price:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(38, 493);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(116, 25);
            this.label7.TabIndex = 18;
            this.label7.Text = "PAYMENT";
            // 
            // btrent
            // 
            this.btrent.Location = new System.Drawing.Point(185, 402);
            this.btrent.Name = "btrent";
            this.btrent.Size = new System.Drawing.Size(103, 29);
            this.btrent.TabIndex = 26;
            this.btrent.Text = "Rent";
            this.btrent.UseVisualStyleBackColor = true;
            this.btrent.Click += new System.EventHandler(this.btrent_Click);
            // 
            // cbitemname
            // 
            this.cbitemname.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbitemname.FormattingEnabled = true;
            this.cbitemname.Location = new System.Drawing.Point(147, 62);
            this.cbitemname.Name = "cbitemname";
            this.cbitemname.Size = new System.Drawing.Size(149, 21);
            this.cbitemname.TabIndex = 15;
            this.cbitemname.SelectedIndexChanged += new System.EventHandler(this.cbitemname_SelectedIndexChanged);
            // 
            // lbreturneditem
            // 
            this.lbreturneditem.FormattingEnabled = true;
            this.lbreturneditem.HorizontalScrollbar = true;
            this.lbreturneditem.Location = new System.Drawing.Point(371, 275);
            this.lbreturneditem.Name = "lbreturneditem";
            this.lbreturneditem.Size = new System.Drawing.Size(325, 108);
            this.lbreturneditem.TabIndex = 14;
            // 
            // lbrenteditem
            // 
            this.lbrenteditem.FormattingEnabled = true;
            this.lbrenteditem.HorizontalScrollbar = true;
            this.lbrenteditem.Location = new System.Drawing.Point(371, 50);
            this.lbrenteditem.Name = "lbrenteditem";
            this.lbrenteditem.Size = new System.Drawing.Size(325, 108);
            this.lbrenteditem.TabIndex = 14;
            // 
            // lbname
            // 
            this.lbname.FormattingEnabled = true;
            this.lbname.HorizontalScrollbar = true;
            this.lbname.Location = new System.Drawing.Point(28, 158);
            this.lbname.Name = "lbname";
            this.lbname.Size = new System.Drawing.Size(268, 225);
            this.lbname.TabIndex = 14;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(76, 12);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(220, 25);
            this.label13.TabIndex = 12;
            this.label13.Text = "ITEM INFORMATION";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(25, 64);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(84, 18);
            this.label14.TabIndex = 7;
            this.label14.Text = "Item Name:";
            // 
            // lbrfidinfo
            // 
            this.lbrfidinfo.FormattingEnabled = true;
            this.lbrfidinfo.Location = new System.Drawing.Point(13, 297);
            this.lbrfidinfo.Name = "lbrfidinfo";
            this.lbrfidinfo.Size = new System.Drawing.Size(400, 160);
            this.lbrfidinfo.TabIndex = 28;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 264);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(135, 13);
            this.label9.TabIndex = 30;
            this.label9.Text = "The list for RFID ShowInfo:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1240, 692);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lbrfidinfo);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.lbrfid);
            this.Controls.Add(this.btrfidopen);
            this.Controls.Add(this.panel2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbaccountemail;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbaccountid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btrfidopen;
        private System.Windows.Forms.Label lbrfid;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btremove;
        private System.Windows.Forms.Button btreturnitems;
        private System.Windows.Forms.Button btmovedown;
        private System.Windows.Forms.Label lbprice;
        private System.Windows.Forms.Button btmoveup;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btrent;
        private System.Windows.Forms.ComboBox cbitemname;
        private System.Windows.Forms.ListBox lbreturneditem;
        private System.Windows.Forms.ListBox lbrenteditem;
        private System.Windows.Forms.ListBox lbname;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ListBox lbrfidinfo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox lbshowreturnlist;
    }
}

