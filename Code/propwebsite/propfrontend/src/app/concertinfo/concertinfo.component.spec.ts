import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConcertinfoComponent } from './concertinfo.component';

describe('ConcertinfoComponent', () => {
  let component: ConcertinfoComponent;
  let fixture: ComponentFixture<ConcertinfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConcertinfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConcertinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
