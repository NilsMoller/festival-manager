import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {RegistrationComponent} from './registration/registration.component';
import {AccountmanagementComponent} from './accountmanagement/accountmanagement.component';
import {LandingComponent} from './landing/landing.component';

const routes: Routes = [
    {path: 'login', component: LoginComponent},
    {path: 'register', component: RegistrationComponent},
    {path: 'manageaccount', component: AccountmanagementComponent},
    {path: 'landing', component: LandingComponent},
    {path: '', redirectTo: '/landing', pathMatch: 'full'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
      RouterModule
  ]
})
export class AppRoutingModule { }
