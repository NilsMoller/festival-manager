import {Component, OnInit} from '@angular/core';
import { AccountmanagementService } from '../accountmanagement.service';
import { Account } from '../classes/account';
import {Ticket} from '../classes/ticket';

@Component({
    selector: 'app-accountmanagement',
    templateUrl: './accountmanagement.component.html',
    styleUrls: ['./accountmanagement.component.css']
})
export class AccountmanagementComponent implements OnInit {
	
	sessionAccount: Account = JSON.parse(sessionStorage.getItem('account'));
	sessionticket: Ticket = JSON.parse(sessionStorage.getItem('ticket'));
    
    isChangingPassword: boolean = false;
    isChangingEmail: boolean = false;
    isChangingPhone: boolean = false;
    isViewingTicket: boolean = false;
    isAddingCredit: boolean = false;
    
    newPhone: string;
    oldPhone: string = this.sessionAccount.Phone;
	newPassword: string;
	newRepeatedPassword: string;
	oldPassword: string;
	creditToAdd: number;
	
    constructor(private AccountManagementService: AccountmanagementService) { }

    ngOnInit() : void {
    }
    
    changePhone(): void {
        this.AccountManagementService.changePhone(this.newPhone);
    }

    changePassword(){
        this.AccountManagementService.changePassword(this.oldPassword, this.newPassword, this.newRepeatedPassword);
    }

    addCredit(){
        this.AccountManagementService.addCredit(this.creditToAdd)
    }
}
