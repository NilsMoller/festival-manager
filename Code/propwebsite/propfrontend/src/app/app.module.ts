import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ScrollToModule} from '@nicky-lenaers/ngx-scroll-to'; //ngx-scroll-to
import { AppRoutingModule } from './app-routing.module'; //routing
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FairinfoComponent } from './fairinfo/fairinfo.component'; //component
import { ConcertinfoComponent } from './concertinfo/concertinfo.component'; //component
import { AccountmanagementComponent } from './accountmanagement/accountmanagement.component'; //component
import { DashboardComponent } from './dashboard/dashboard.component'; //component
import { BuyticketComponent } from './buyticket/buyticket.component'; //component
import { AppComponent } from './app.component'; //component
import { LoginComponent } from './login/login.component'; //component
import { RegistrationComponent } from './registration/registration.component';
import { LandingComponent } from './landing/landing.component';
import {HttpClient, HttpClientModule, HttpHandler} from '@angular/common/http'; //component

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        RegistrationComponent,
        FairinfoComponent,
        ConcertinfoComponent,
        AccountmanagementComponent,
        DashboardComponent,
        BuyticketComponent,
        LandingComponent
    ],
    imports: [
        BrowserModule,
        ScrollToModule.forRoot(),
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule
    ],
    providers: [HttpClientModule],
    bootstrap: [AppComponent]
})
export class AppModule { }
