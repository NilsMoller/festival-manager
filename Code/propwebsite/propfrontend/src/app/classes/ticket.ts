
export class Ticket {
    AmountOfPeople: number;
    DateOfPurchase: string;
    CampingId: number;
    Price: number;
    OtherEmails: string[];
    
    /*
    database stored ticket:
        id - auto
        date+time of purchase
        campingid
        amountofpeople
        otheremails
     */
    constructor(amountOfPeople: number, price: number, spotNo?: number, OtherEmails?: string[]) {
        this.AmountOfPeople = amountOfPeople;
        this.CampingId = spotNo;
        if (OtherEmails.length != 0){
            this.OtherEmails = OtherEmails;
        }
        this.Price = price;
        this.DateOfPurchase = new Date().toLocaleString();
    }
}
