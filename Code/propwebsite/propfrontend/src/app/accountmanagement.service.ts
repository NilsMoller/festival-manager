import { Injectable } from '@angular/core';
import { Account} from './classes/account';
import {ApiInteractionService} from './api-interaction.service';

@Injectable({
    providedIn: 'root'
})
export class AccountmanagementService {
    
    sessionAccount: Account = JSON.parse(sessionStorage.getItem('account'));
    
    constructor(private api: ApiInteractionService) { }
    
    changePhone(newPhone: string): void {
        this.sessionAccount.Phone = newPhone; //updates local phone
        sessionStorage.setItem('account', JSON.stringify(this.sessionAccount)); //updates session phone
        this.sessionAccount = JSON.parse(sessionStorage.getItem('account'));
        this.api.updateAccountPhone(this.sessionAccount).subscribe( //updates db phone
        () => {
            window.alert('Changed phone.');
            location.replace('/manageaccount');
        });
    }
    
    changePassword(oldPassword: string, newPassword: string, repeatedPassword: string): void{
        if (newPassword != repeatedPassword){
            window.alert("New passwords don't match.");
            return;
        }
        else if (oldPassword != this.sessionAccount.Password) {
            window.alert("Old password doesn't match");
            return;
        }
        this.sessionAccount.Password = newPassword; //updates local password
        sessionStorage.setItem('account', JSON.stringify(this.sessionAccount)); //updates session password
        this.sessionAccount = JSON.parse(sessionStorage.getItem('account'));
        this.api.updateAccountPassword(this.sessionAccount).subscribe( //updates db password
            () => {
                window.alert('Changed password.');
                location.replace('/manageaccount');
            });
    }

    addCredit(creditToAdd: number): void {
        if (typeof creditToAdd != 'number') {
            alert('Please fill in a number.');
            return;
        }
        else if (!confirm('Are you sure?')){
            console.log('(addCredit() - accountmanagementservice) user chose cancel');
            return;
        } //check for customer certainty
        this.sessionAccount.CreditStored = this.sessionAccount.CreditStored += creditToAdd; //updates local credit
        sessionStorage.setItem('account', JSON.stringify(this.sessionAccount)); //updates session credit
        this.sessionAccount = JSON.parse(sessionStorage.getItem('account')); //updates local sessionaccount
        this.api.addCredit(this.sessionAccount).subscribe( //updates db credit
            () => {
                window.alert('Done!');
                location.replace('/manageaccount');
            });
    }
}
