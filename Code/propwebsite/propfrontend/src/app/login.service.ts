import {Injectable} from '@angular/core';
import {Account} from './classes/account';
import {ApiInteractionService} from './api-interaction.service';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class LoginService {
    
    constructor(private api: ApiInteractionService) {
    }
    
    public logIn(email, password): Observable<Account>{
        if (email == null || password == null){
            alert('Please fill in all fields');
            return;
        }
        return this.api.getAccount(email, password).pipe(
            tap(a => {
                if (a == undefined){ //no account found
                    console.log('(logIn() - loginService) no account with that email/password combination');
                    alert("Didn't find an account with that email/password combination");
                    return null;
                }
                else { //account found. setting sessionstorage and reloading the page
                    console.log('(logIn() - loginService) found account');
                    sessionStorage.setItem('account', JSON.stringify(a)); //set session variable
                    if (a.TicketId != null){ //if account has ticket
                        this.api.getTicket(a.TicketId).subscribe(t => { //get that ticket
                            console.log('(logIn() - loginService) found ticket');
                            sessionStorage.setItem('ticket', JSON.stringify(t)); //set session ticket to found ticket
                        });
                    }
                    alert('Logged in as ' + a.Name + '.');
                    location.replace('/landing');
                    return a;
                }
            })
        );
    }
    
    public logOut(): void{
        sessionStorage.clear();
        alert('Logged out.');
        if (location.pathname == '/manageaccount')
            location.replace('/landing');
        else
            location.reload();
    }
}
