import { Component } from '@angular/core';
import {BuyticketService} from '../buyticket.service';
import {Ticket} from '../classes/ticket';

@Component({
    selector: 'app-buyticket',
    templateUrl: './buyticket.component.html',
    styleUrls: ['./buyticket.component.css']
})

export class BuyticketComponent {
    amountoftickets: number = 1;
    isCamping: boolean = false;
    price: number = this.getTotalPrice();
    email2: string;
    email3: string;
    email4: string;
    email5: string;
    email6: string;
    temp: any;
    
    constructor(private BuyticketService: BuyticketService) { }
    
    getTotalPrice(): number{
        return (this.price = this.BuyticketService.getTotalPrice(this.isCamping, Number.parseInt(this.amountoftickets.toString())));
    }
    buyTicket(): Ticket{
         let otheremails: string[] = [];
         /*region add to array*/
         if (2 == this.amountoftickets){
             otheremails.push(this.email2);
         }
         else if (3 == this.amountoftickets){
             otheremails.push(this.email2);
             otheremails.push(this.email3);
         }
         else if (4 == this.amountoftickets){
             otheremails.push(this.email2);
             otheremails.push(this.email3);
             otheremails.push(this.email4);
         }
         else if (5 == this.amountoftickets){
             otheremails.push(this.email2);
             otheremails.push(this.email3);
             otheremails.push(this.email4);
             otheremails.push(this.email5);
         }
         else if (6 == this.amountoftickets){
             otheremails.push(this.email2);
             otheremails.push(this.email3);
             otheremails.push(this.email4);
             otheremails.push(this.email5);
             otheremails.push(this.email6);
         }
         /*endregion*/
         return this.BuyticketService.buyTicket(this.amountoftickets, this.isCamping, this.price, otheremails);
    }
}
