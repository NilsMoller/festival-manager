<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class account extends Model
{
    public $timestamps = false;
    protected $table = 'accounts';
    protected $fillable = [
        'Email',
        'Name',
        'Password',
        'Gender',
        'Phone',
        'CheckedInEvent',
        'CheckedInCamping',
        'CreditStored',
        'Address',
        'DateOfBirth',
        'RFID',
        'TicketId',
        'PaidForTicket'
    ];
}
