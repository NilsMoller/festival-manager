<?php

namespace App\Http\Controllers;

use App\camping;
use App\Http\Resources\CampingResource;
use Illuminate\Http\Request;
use DB;

class CampingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $campings = camping::paginate(15);
        return new CampingResource($campings);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $camping = new camping();
        $camping->isAvailable = $request->input('isAvailable');
        $camping->save();
        return new CampingResource($camping);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $camping = camping::findOrFail($id);
        return new CampingResource($camping);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('post')
            ->where('Id', $id)
            ->update(['ísAvailable' => $request->input('isAvailable')]);
        return new CampingResource(camping::find($id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $camping = camping::findOrFail($id);
        DB::table('campings')->where('Id', '=', $id)->delete();
        return new CampingResource($camping);
    }

    /**
     * returns the first free spot in the campings table
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function freespot() //tries to find first id with isAvailable = 1, if it doesn't find one, return null
    {
        $id = DB::table('campings')
            ->where('isAvailable', '=', '1')
            ->min('Id');
            //$camping = camping::find($id);
            return $id;
    }
}
